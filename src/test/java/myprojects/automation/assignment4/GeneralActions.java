package myprojects.automation.assignment4;

import com.google.common.base.Predicate;
import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.utils.Properties;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;



/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    private WebElement loginField;
    private WebElement passwordField;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        // TODO implement logging in to Admin Panel
        driver.get(Properties.getBaseAdminUrl());

        // Form Fields initialize and submit
        loginField = driver.findElement(By.id("email"));
        passwordField = driver.findElement(By.name("passwd"));
        loginField.sendKeys(login);
        passwordField.sendKeys(password);
        passwordField.submit();

        WebElement catalog = driver.findElement(By.id("subtab-AdminCatalog"));
        Actions actions = new Actions(driver);
        actions.moveToElement(catalog).build().perform();

        wait.until(ExpectedConditions. visibilityOf(catalog.findElements(By.cssSelector("li")).get(1)));
        catalog.findElements(By.cssSelector("li")).get(0).click();


    }

    public void createProduct(ProductData newProduct) {
        // TODO implement product creation scenario

        driver.findElement(By.id("page-header-desc-configuration-add")).click();

        driver.findElement(By.id("form_step1_name_1")).sendKeys(newProduct.getName());
        driver.findElement(By.id("form_step1_qty_0_shortcut")).sendKeys(String.valueOf(newProduct.getQty()));

        driver.findElement(By.id("form_step1_price_ttc_shortcut")).sendKeys(String.valueOf(newProduct.getPrice()));

        driver.findElement(By.tagName("html")).sendKeys(Keys.chord(Keys.CONTROL, "o"));

        // waiting for load system window
        wait.until((Predicate) webDriver -> driver.findElement(By.id("growls")).getText().contains("обновлены"));
        // close system window
        driver.findElement(By.id("growls")).findElement(By.cssSelector(".growl-close")).click();

        // Click save button
        driver.findElement(By.cssSelector(".btn.btn-primary.js-btn-save")).click();

        // waiting for load system window
        wait.until((Predicate) webDriver -> driver.findElement(By.cssSelector(".growl-message")).getText().contains("обновлены"));
        // close system window
        driver.findElement(By.id("growls")).findElement(By.cssSelector(".growl-close")).click();
    }

    public void openShop () {
        ((JavascriptExecutor)driver).executeScript("window.open()"); // new tab

        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1)); //switches to new tab
        driver.get(Properties.getBaseUrl()); // load prestashop home page

        driver.findElement(By.xpath("//*[@id=\"content\"]/section/a")).click(); // click on "Vse tovary :)"
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {
        // TODO implement generic method to wait until page content is loaded



        // wait.until(...);
        // ...
    }
}
