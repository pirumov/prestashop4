package myprojects.automation.assignment4;

import myprojects.automation.assignment4.utils.Properties;
import myprojects.automation.assignment4.utils.logging.EventHandler;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {
    protected EventFiringWebDriver driver;
    protected GeneralActions actions;

    /**
     *
     * @param browser Driver type to use in tests.
     *
     * @return New instance of {@link WebDriver} object.
     */
    private WebDriver getDriver(String browser) {
//        String browser = Properties.getBrowser();
        String os = Properties.getOS();

        //OS check only for use drivers for Mac or Windows

        if (!os.equals("Mac OS X")) {
            switch (browser) {
                case "firefox":
                    System.setProperty(
                            "webdriver.gecko.driver",
                            getResource("/geckodriver.exe"));
                    return new FirefoxDriver();
                case "ie":
                case "internet explorer":
                    System.setProperty(
                            "webdriver.ie.driver",
                            getResource("/IEDriverServer.exe"));
                    return new InternetExplorerDriver();
                case "chrome":
                default:
                    System.setProperty(
                            "webdriver.chrome.driver",
                            getResource("/chromedriver.exe"));
                    return new ChromeDriver();
            }
        } else {
            switch (browser) {
                case "firefox":
                    System.setProperty(
                            "webdriver.gecko.driver",
                            getResource("/geckodriver")); //MacOS
                    return new FirefoxDriver();
                case "chrome":
                default:
                    System.setProperty(
                            "webdriver.chrome.driver",
                            getResource("/chromedriver")); //MacOS
                    return new ChromeDriver();
            }
        }
    }

    /**
     * @param resourceName The name of the resource
     * @return Path to resource
     */
    private String getResource(String resourceName) {
        try {
           return Paths.get(BaseScript.class.getResource(resourceName).toURI()).toFile().getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return resourceName;
    }

    /**
     * Prepares {@link WebDriver} instance with timeout and browser window configurations.
     *
     * Driver type is based on passed parameters to the automation project,
     * creates {@link ChromeDriver} instance by default.
     *
     */

    @Parameters({"browser"})
    @BeforeClass

    // TODO use parameters from pom.xml to pass required browser type
    public void setUp(String browser ) {
        driver = new EventFiringWebDriver(getDriver(browser));
        driver.register(new EventHandler());

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        actions = new GeneralActions(driver);
    }

    /**
     * Closes driver instance after test class execution.
     */
    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
