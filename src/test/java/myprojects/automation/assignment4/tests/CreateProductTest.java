package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseScript;
import myprojects.automation.assignment4.model.ProductData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

public class CreateProductTest extends BaseScript {

    ProductData productData = ProductData.generate();

    @DataProvider // set auth data
    public Object[][] getLogin() {
        return new String[][]{
                {"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}
        };
    }

    @Test(dataProvider = "getLogin") // login to admin panel
    public void login(String login, String password) {

        actions.login(login, password);
    }

    @Test(dependsOnMethods = "login") // create and save new product
    public void createProductTest() {

        actions.createProduct(productData);
    }

    @Test(dependsOnMethods = "openPresta")
    public void testProductPresent() {

        List<WebElement> articles = driver.findElements(By.tagName("article")); // find all products

        for (WebElement article : articles) {

            String productName = article.findElement(By.cssSelector(".product-description"))
                    .findElement(By.tagName("a")).getText();

            if (productName.equals(productData.getName())) {
                Reporter.log("Created product present! <br />");

                article.findElement(By.cssSelector(".product-description"))
                        .findElement(By.tagName("a")).click();

                // get price from site
                String price = driver.findElement(By.xpath("//span[@itemprop='price']")).getText();
                price = price.split(" ")[0];

                // get qty from site
                String qty = driver.findElement(By.xpath("//*[@id=\"product-details\"]/div[1]/span")).getText();
                qty = qty.split(" ")[0];

                // Name check
                Assert.assertEquals(productData.getName().toUpperCase(),
                        driver.findElement(By.xpath("//h1[@itemprop='name']")).getText(),
                        "Product Name isn't equal");
                Reporter.log("Name OK! <br />");

                // Price check
                Assert.assertEquals(productData.getPrice().toString(), price,
                        "Price isn't equal");
                Reporter.log("Price OK! <br />");

                // Qty check
                Assert.assertEquals(productData.getQty().toString(), qty,
                        "Qty isn't equal");
                Reporter.log("Qty OK! <br />");
            } else {
                Reporter.log("Created product doesn't present! <br />");
            }

        }


    }


    @Test(dependsOnMethods = "createProductTest") // open front page in new tab
    public void openPresta() {
        actions.openShop();
    }

}
